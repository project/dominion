<?php

function dominion_alias_form_dominion_form_alter(&$form, &$form_state) {
  $form['dominion_alias'] = array(
    '#type' => 'fieldset',
    '#title' => t('Aliases'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $aliases = array();
  if (isset($form_state['values'])) {
    $aliases = _dominion_alias_get_configuration($form_state['values']);
  }
  elseif ($form['#domain_id']) {
    // Load aliases from database
    $aliases = _dominion_alias_get_configuration_from_database($form['#domain_id']);
  }

  $domain_suffix = variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT);

  $form['dominion_alias']['aliases'] = array(
    '#prefix' => '<div id="dominion-alias-aliases">',
    '#suffix' => '</div>',
  );

  $num = count($aliases) + 2;
  for ($i = 0; $i < $num; $i++) {
    $form['dominion_alias']['aliases']["dominion_alias_{$i}"] = array(
      '#type' => 'textfield',
      '#title' => t('Alias'),
      '#default_value' => !empty($aliases[$i]) ? $aliases[$i]['alias'] : '',
      '#description' => t('Part before %domain, i.e. "@example_subdomain" for %example_domain. ', array('%domain' => $domain_suffix, '@example_subdomain' => t('blog'), '%example_domain' => t('blog') . $domain_suffix)),
    );
    if (variable_get(DOMINION_VAR_ALLOW_TLD, DOMINION_VAR_ALLOW_TLD_DEFAULT)) {
      $form['dominion_alias']['aliases']["dominion_alias_{$i}_domain"] = array(
        '#type' => 'checkbox',
        '#title' => t('This is a full domainname instead of a subdomain of %domain.', array('%domain' => $domain_suffix)),
        '#default_value' => !empty($aliases[$i]) ? $aliases[$i]['domain'] : '',
      );
    }
    $form['dominion_alias']['aliases']["dominion_alias_{$i}_redirect"] = array(
      '#type' => 'checkbox',
      '#title' => t('Redirect this alias'),
      '#default_value' => !empty($aliases[$i]) ? $aliases[$i]['redirect'] : '',
    );
  }

  $form['dominion_alias']['dominion_alias_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add another alias'),
    '#ahah' => array(
      'path' => 'dominion_form_ahah/dominion_alias/aliases',
      'wrapper' => 'dominion-alias-aliases',
      'effect' => 'fade',
      'method' => 'replace',
    ),
  );
}

/**
 * Implements hook_dominion().
 */
function dominion_alias_dominion($op, $domain_id, &$a3 = NULL) {
  switch ($op) {
    case 'update':
      $sql = 'DELETE FROM {domain_alias} WHERE domain_id = %d';
      db_query($sql, $domain_id);
    case 'insert':
      $aliases = _dominion_alias_get_configuration($a3);
      $domain_suffix = variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT);
      foreach ($aliases as $alias) {
        $pattern = $alias['domain'] ? $alias['alias'] : $alias['alias'] . $domain_suffix;
        $redirect = (int) $alias['redirect'];
        $sql = 'INSERT INTO {domain_alias} (domain_id, pattern, redirect) VALUES (%d, \'%s\', %d)';
        db_query($sql, $domain_id, $pattern, $redirect);
      }
      break;
    case 'delete':
      // Aliases are already deleted by the domain module.
      break;
  }
}

function _dominion_alias_get_configuration($values) {
  $aliases = array();
  foreach ($values as $name => $value) {
    if (preg_match('/^dominion_alias_([0-9]+)$/s', $name, $match)) {
      $id = $match[1];
      if (!empty($value)) {
        $aliases[] = array(
          'alias' => $value,
          'redirect' => !empty($values["dominion_alias_{$id}_redirect"]),
          'domain' => !empty($values["dominion_alias_{$id}_domain"]),
        );
      }
    }
  }
  return $aliases;
}

function _dominion_alias_get_configuration_from_database($domain_id) {
  $aliases = array();
  $domain_suffix = variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT);

  $sql = 'SELECT pattern, redirect
  FROM {domain_alias}
  WHERE domain_id = %d
  ORDER BY alias_id';
  $res = db_query($sql, $domain_id);
  while ($rec = db_fetch_array($res)) {
    $regexp = '/' . preg_quote($domain_suffix) . '$/si';
    if (preg_match($regexp, $rec['pattern'])) {
      $pattern = preg_replace($regexp, '', $rec['pattern']);
      $domain = FALSE;
    }
    else {
      $pattern = $rec['pattern'];
      $domain = TRUE;
    }
    $aliases[] = array(
      'alias' => $pattern,
      'redirect' => (bool) $rec['redirect'],
      'domain' => $domain,
    );
  }

  return $aliases;
}

