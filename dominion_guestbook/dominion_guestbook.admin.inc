<?php

function dominion_guestbook_admin_settings() {
  $form = array();

  $form[DOMINION_GUESTBOOK_VAR_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => variable_get(DOMINION_GUESTBOOK_VAR_PATH, DOMINION_GUESTBOOK_VAR_PATH_DEFAULT),
    '#required' => TRUE,
    '#description' => t('Path for the guestbook.'),
  );

  $form[DOMINION_GUESTBOOK_VAR_TITLE] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get(DOMINION_GUESTBOOK_VAR_TITLE, DOMINION_GUESTBOOK_VAR_TITLE_DEFAULT),
    '#required' => TRUE,
    '#description' => t('Title to use for the guestbook nodes.'),
  );

  $options = node_get_types('names');
  $form[DOMINION_GUESTBOOK_VAR_NODETYPE] = array(
    '#type' => 'select',
    '#title' => t('Nodetype for guestbook'),
    '#options' => $options,
    '#default_value' => variable_get(DOMINION_GUESTBOOK_VAR_NODETYPE, ''),
  );

  $form['#submit'][] = 'dominion_guestbook_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Form submit handler.
 */
function dominion_guestbook_admin_settings_submit($form, &$form_state) {
  dominion_path(DOMINION_GUESTBOOK_VAR_PATH, $form_state['values'][DOMINION_GUESTBOOK_VAR_PATH], TRUE);
  menu_rebuild();
}