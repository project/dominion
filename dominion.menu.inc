<?php

function _dominion_menu_get_name($domain_id) {
  return str_replace('%', $domain_id, DOMINION_MENU_NAME);
}

function _dominion_menu_create($domain_id, $functions) {
  $menu = _dominion_menu_get_name($domain_id);
  $configuration = variable_get(DOMINION_VAR_DEFAULT_MENU, DOMINION_VAR_DEFAULT_MENU_DEFAULT);

  // Add a menu using the menu_edit_menu form
  $menu_edit_menu_form_state = array(
    'values' => array(
      'menu_name' => $menu,
      'title' => substr(t('Menu for @name', array('@name' => $name)), 0, MENU_MAX_MENU_NAME_LENGTH_UI),
      'description' => '',
    )
  );
  module_load_include('inc', 'menu', 'menu.admin');
  drupal_execute('menu_edit_menu', $menu_edit_menu_form_state, 'add');

  // Add menu items.
  $weight = 0;
  $items = array_filter(array_map('trim', explode("\n", $configuration)));
  foreach ($items as $item) {
    $item = array_filter(array_map('trim', explode('|', $item)));
    if (isset($item[2]) && empty($functions[$item[2]])) {
      // This subsite does not have the required function enabled.
      continue;
    }
    ++$weight;
    $item = array(
      'link_path' => $item[0],
      'link_title' => $item[1],
      'weight' => $weight,
      'menu_name' => "menu-$menu",
    );
    menu_link_save($item);
  }
}

function _dominion_menu_delete($domain_id) {
  // Delete menu using the menu_delete_menu_confirm form.
  $menu = _dominion_menu_get_name($domain_id);
  $sql = 'SELECT * FROM {menu_custom} WHERE menu_name = \'%s\'';
  $res = db_query($sql, $menu);
  if ($menu = db_fetch_array($res)){
    $menu_delete_menu_confirm_form_state = array('#values' => array());
    module_load_include('inc', 'menu', 'menu.admin');
    menu_delete_menu_confirm_submit(array('#menu' => $menu), $menu_delete_menu_confirm_form_state);
  }
}