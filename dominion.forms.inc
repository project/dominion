<?php

/**
 * Form to deploy a new subsite or edit an existing subsite.
 */
function dominion_form(&$form_state, $domain_id = NULL){
  $form = array();

  // This is an edit form, store data in form
  if (!empty($domain_id)){
    $form['#domain_id'] = $domain_id;
    if (!$subsite = dominion_get_info($domain_id)) {
      drupal_not_found();
    }
    $form['#action'] = url('admin/build/dominion/list/' . $domain_id . '/edit');
  }
  else {
    $form['#action'] = url('admin/build/dominion/add');
  }
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => !is_null($domain_id) ? $subsite->name : '',
    '#description' => t('Name to use for this subsite.'),
  );

  $domain_suffix = variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT);
  $allow_tld = variable_get(DOMINION_VAR_ALLOW_TLD, DOMINION_VAR_ALLOW_TLD_DEFAULT);
  if ($allow_tld || ($domain_id && $subsite->domain_type == 'domain')) {
    $options = array(
      'subdomain' => t('Use a subdomain of %domain', array('%domain' => $domain_suffix)),
      'domain' => t('Use a custom domainname'),
    );
    $form['domain_type'] = array(
      '#type' => 'radios',
      '#title' => t('Domain type'),
      '#options' => $options,
      '#default_value' => !is_null($domain_id) ? $subsite->domain_type : 'subdomain',
    );
    $form['domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Domainname'),
      '#size' => 40,
      '#default_value' => !is_null($domain_id) ? $subsite->domain_type == 'domain' ? $subsite->subdomain : '' : '',
      '#description' => t('The full domainname, i.e.: "example.com"', array('%domain' => $domain_suffix, '@example_subdomain' => t('blog'), '%example_domain' => t('blog') . $domain_suffix)),
    );
    drupal_add_js(drupal_get_path('module', 'dominion') . '/dominion.forms.js', 'module');
  }

  $form['subdomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdomain'),
    '#required' => $allow_tld || (!is_null($domain_id) && $subsite->domain_type == 'domain') ? FALSE : TRUE,
    '#size' => 40,
    '#default_value' => !is_null($domain_id) ? $subsite->domain_type == 'subdomain' ? $subsite->subdomain : '' : '',
    '#description' => t('Part before %domain, i.e. "@example_subdomain" for %example_domain. ', array('%domain' => $domain_suffix, '@example_subdomain' => t('blog'), '%example_domain' => t('blog') . $domain_suffix)),
  );

  if (variable_get(DOMINION_VAR_ALLOW_CUSTOM_FRONTPAGE, DOMINION_VAR_ALLOW_CUSTOM_FRONTPAGE_DEFAULT)) {
    $form['frontpage'] = array(
      '#type' => 'textfield',
      '#title' => t('Frontpage'),
      '#default_value' => !is_null($domain_id) ? $subsite->frontpage : variable_get(DOMINION_VAR_DEFAULT_FRONTPAGE, ''),
      '#size' => 40,
      '#description' => t('System path of the frontpage to use for this subsite. Leave empty to use site default.'),
    );
  }

  if ($options = dominion_get_all_functions()) {
    $form['functions'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available functions'),
      '#default_value' => !is_null($domain_id) ? drupal_map_assoc($subsite->available_functions) : array(),
      '#options' => $options,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 50,
  );

  return $form;
}

/**
 * Form validate for dominion_form.
 */
function dominion_form_validate($form, &$form_state) {
  $domain_id = isset($form['#domain_id']) ? $form['#domain_id'] : -1;

  // Get hostname for the subdomain, we call this "subdomain" to follow the naming of the domain module.
  $domain_type = $form_state['values']['domain_type'];
  $domain_suffix = variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT);
  switch ($domain_type) {
    case 'subdomain':
      $subdomain = $form_state['values']['subdomain'] . $domain_suffix;
      break;
    case 'domain':
      $subdomain = $form_state['values']['domain'];
      break;
  }

  if (!preg_match('/^[a-z0-9\\-\\_\\.]+(\\:[0-9]+)?$/', $subdomain)){
    form_set_error($domain_type, t('The specified domain is invalid.'));
  } elseif (_dominion_domain_exists($subdomain, $domain_id)){
    form_set_error($domain_type, t('The specified domain is already in use.'));
  }
}

/**
 * Form submit for dominion_form.
 */
function dominion_form_submit($form, &$form_state) {
  // Check for AHAH callback.
  if (arg(0) == 'dominion_form_ahah') {
    $form_state['rebuild'] = TRUE;
    return $form_state['values'];
  }

  $name = $form_state['values']['name'];

  // Get hostname for the subdomain, we call this "subdomain" to follow the naming of the domain module.
  $domain_type = isset($form_state['values']['domain_type']) ? $form_state['values']['domain_type'] : 'subdomain';
  $domain_suffix = variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT);
  switch ($domain_type) {
    case 'subdomain':
      $subdomain = $form_state['values']['subdomain'] . $domain_suffix;
      break;
    case 'domain':
      $subdomain = $form_state['values']['domain'];
      break;
  }

  $frontpage = empty($form_state['values']['frontpage']) ? variable_get(DOMINION_VAR_DEFAULT_FRONTPAGE, '') : $form_state['values']['frontpage'];

  if (!isset($form['#domain_id'])) {
    // Call functions which actually creates the subsite.
    if ($domain_id = _dominion_create_domain($subdomain, $name)) {
      $menu = str_replace('%', $domain_id, DOMINION_MENU_NAME);
      module_load_include('inc', 'dominion', 'dominion.menu');
      _dominion_menu_create($domain_id, $form_state['values']['functions']);
      domain_conf_variable_set($domain_id, 'site_frontpage', $frontpage);
      domain_conf_variable_set($domain_id, 'menu_secondary_links_source', "menu-$menu");
    } else {
      drupal_set_message(t('Error while creating new domain. The new subsite was not created.'), 'error');
      return;
    }
  } else {
    $domain_id = $form['#domain_id'];
    $menu = str_replace('%', $domain_id, DOMINION_MENU_NAME);
    _dominion_update_domain($domain_id, $subdomain, $name);
    domain_conf_variable_set($domain_id, 'site_frontpage', $frontpage);
    domain_conf_variable_set($domain_id, 'menu_secondary_links_source', "menu-$menu");
  }

  // Update function list.
  if (isset($form_state['values']['functions'])) {
    foreach ($form_state['values']['functions'] as $function => $value) {
      if ($value) {
        $sql = 'SELECT * FROM {dominion_function} WHERE domain_id = %d AND name = \'%s\'';
        $res = db_query($sql, $domain_id, $function);
        if (!db_fetch_array($res)) {
          $sql = 'INSERT INTO {dominion_function} (domain_id, name) VALUES (%d, \'%s\')';
          db_query($sql, $domain_id, $function);
        }
      }
      else {
        $sql = 'DELETE FROM {dominion_function} WHERE domain_id = %d AND name = \'%s\'';
        db_query($sql, $domain_id, $function);
      }
    }
  }

  if ($frontpage_nodetype = variable_get(DOMINION_VAR_FRONTPAGE_NODETYPE, '')) {
    dominion_save_node($domain_id, $frontpage_nodetype, $name);
  }

  module_invoke_all('dominion', isset($form['#domain_id']) ? 'update' : 'insert', $domain_id, $form_state['values']);

  cache_clear_all("info:$domain_id", 'cache_dominion');

  if (isset($form['#domain_id'])){
    drupal_set_message(t('The changes to @name were saved succesfully.', array('@name' => $name)));
  } else {
    drupal_set_message(t('The subdomain @name was created succesfully.', array('@name' => $name)));
  }

  if ($custom_redirect = dominion_form_set_redirect()) {
    $form_state['redirect'] = $custom_redirect;
  }
  else {
    $form_state['redirect'] = 'admin/build/dominion';
  }
}

/**
 * Set redirect after form submit.
 */
function dominion_form_set_redirect($dst = NULL) {
  static $redir = NULL;
  !$dst or $redir = $dst;
  return $redir;
}

/**
 * AHAH callback.
 */
function dominion_form_ahah() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $x = 1;
  $ahah_part = $form;
  while ($container = arg($x)) {
    $ahah_part = $ahah_part[$container];
    $x++;
  }
  unset($ahah_part['#prefix'], $ahah_part['#suffix']);
  $output = theme('status_messages') . drupal_render($ahah_part);

  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Form to delete a subsite or edit an existing subsite.
 */
function dominion_delete_form(&$form_state, $domain_id = null){
  $form = array();

  $form['#domain_id'] = $domain_id;
  if (!$subsite = dominion_get_info($domain_id)) {
    drupal_not_found();
    exit;
  }
  
  $form['text'] = array(
    '#value' => t('Are you sure you want to delete the subsite %name and all of its contents?', array('%name' => $subsite->name)),
  );

  $form['buttons'] = array(
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
  );

  $form['buttons']['cancel'] = array(
    '#value' => l(t('cancel'), 'admin/build/dominion'),
  );

  return $form;
}

/**
 * Submit handler for dominion_delete_form().
 */
function dominion_delete_form_submit($form, &$form_state) {
  if (!$subsite = dominion_get_info($form['#domain_id'])) {
    drupal_not_found();
    return;
  }

  // Delete domain record.
  _dominion_delete_domain($form['#domain_id']);
  
  module_load_include('inc', 'dominion', 'dominion.menu');
  _dominion_menu_delete($domain_id);

  // Invoke other modules.
  module_invoke_all('dominion', 'delete', $domain_id, $subsite);

  // Cleanup dominion cache for this domain.
  cache_clear_all("info:$domain_id", 'cache_dominion');

  drupal_set_message(t('The subsite %name has been deleted.', array('%name' => $subsite->name)));
  
  $form_state['redirect'] = 'admin/build/dominion';
}