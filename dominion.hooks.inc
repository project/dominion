<?php

/**
 * Hook for actions taken on subsites handled by dominion.
 *
 * @param string $op
 * @param int $domain_id
 * @param mixed $a3
 */
function hook_dominion($op, $domain_id, &$a3 = NULL) {
  switch ($op) {
    case 'insert':
      // $a3 is an associative array with all values from dominion_form.
      break;
    case 'update':
      // $a3 is an associative array with all values from dominion_form.
      break;
    case 'delete':
      // $a3 is an object from dominion_get_info().
      break;
    case 'load':
      // $a3 is an object from dominion_get_info(), you may alter this object here.
      break;
  }
}

/**
 * Hook for defining functions available in dominion subsites.
 *
 * @return array
 */
function hook_dominion_functions() {
  return array(
    'forum' => t('Forum'),
    'blog' => t('Blog'),
  );
}
