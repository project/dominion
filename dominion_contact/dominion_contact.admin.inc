<?php

function dominion_contact_admin_settings() {
  $form = array();

  $form[DOMINION_CONTACT_VAR_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_PATH, DOMINION_CONTACT_VAR_PATH_DEFAULT),
    '#required' => TRUE,
    '#description' => t('Path for the contactform.'),
  );

  $form[DOMINION_CONTACT_VAR_TITLE] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_TITLE, DOMINION_CONTACT_VAR_TITLE_DEFAULT),
    '#required' => TRUE,
    '#description' => t('Title to use for the contactform nodes.'),
  );

  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
    '#collapsible' => FALSE,
  );

  $form['fields'][DOMINION_CONTACT_VAR_FIELD_NAME] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_FIELD_NAME, DOMINION_CONTACT_VAR_FIELD_NAME_DEFAULT),
    '#description' => t('Title to use for the name field. Leave blank to disable this field.'),
  );

  $form['fields'][DOMINION_CONTACT_VAR_FIELD_EMAIL] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_FIELD_EMAIL, DOMINION_CONTACT_VAR_FIELD_EMAIL_DEFAULT),
    '#description' => t('Title to use for the e-mail field. Leave blank to disable this field.'),
  );

  $form['fields'][DOMINION_CONTACT_VAR_FIELD_SUBJECT] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_FIELD_SUBJECT, DOMINION_CONTACT_VAR_FIELD_SUBJECT_DEFAULT),
    '#description' => t('Title to use for the subject field. Leave blank to disable this field.'),
  );

  $form['fields'][DOMINION_CONTACT_VAR_FIELD_MESSAGE] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_FIELD_MESSAGE, DOMINION_CONTACT_VAR_FIELD_MESSAGE_DEFAULT),
    '#description' => t('Title to use for the message field. Leave blank to disable this field.'),
  );

  $form[DOMINION_CONTACT_VAR_CONFIRMATION] = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation message'),
    '#default_value' => variable_get(DOMINION_CONTACT_VAR_CONFIRMATION, DOMINION_CONTACT_VAR_CONFIRMATION_DEFAULT),
    '#required' => TRUE,
    '#description' => t('Message which is displayed to the user after submitting the contactform.'),
  );

  $form[DOMINION_CONTACT_VAR_BODY] = array(
    '#type' => 'textarea',
    '#title' => t('Bodytext'),
    '#default_value' => filter_xss_admin(variable_get(DOMINION_CONTACT_VAR_BODY, DOMINION_CONTACT_VAR_BODY_DEFAULT)),
    '#description' => t('Bodytext for the node.'),
  );

  $form['#submit'][] = 'dominion_contact_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Form submit handler.
 */
function dominion_contact_admin_settings_submit($form, &$form_state) {
  dominion_path(DOMINION_CONTACT_VAR_PATH, $form_state['values'][DOMINION_CONTACT_VAR_PATH], TRUE);
  menu_rebuild();
}