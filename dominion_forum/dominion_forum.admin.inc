<?php

function dominion_forum_admin_settings() {
  $form = array();

  $form[DOMINION_FORUM_VAR_DEFAULT_FORUMS] = array(
    '#type' => 'textarea',
    '#title' => t('Default forums'),
    '#default_value' => variable_get(DOMINION_FORUM_VAR_DEFAULT_FORUMS, DOMINION_FORUM_VAR_DEFAULT_FORUMS_DEFAULT),
    '#wysiwyg' => FALSE,
    '#description' => t('Format: "[container]", "(forum)", "forum description" - one part per line, in hierachical order. You may use "$name" which will be replaced by the subsite name.'),
  );
  
  return system_settings_form($form);
}
