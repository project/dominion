<?php

function dominion_admin_overview() {
  $output = '';

  $table = array();
  if (isset($_GET['search'])) {
    $sql = 'SELECT d.domain_id, d.sitename, d.subdomain, d.scheme
    FROM {dominion} dmnn
    JOIN {domain} d ON (d.domain_id = dmnn.domain_id)
    WHERE d.sitename LIKE \'%s\'
    OR d.subdomain lIKE \'%s\'
    ORDER BY d.sitename ASC';
    $args = array('%' . $_GET['search'] . '%', '%' . $_GET['search'] . '%');
  }
  else {
    $sql = 'SELECT d.domain_id, d.sitename, d.subdomain, d.scheme
    FROM {dominion} dmnn
    JOIN {domain} d ON (d.domain_id = dmnn.domain_id)
    ORDER BY d.sitename ASC';
    $args = array();
  }
  $items = variable_get(DOMINION_VAR_ITEMS_IN_OVERVIEW, DOMINION_VAR_ITEMS_IN_OVERVIEW_DEFAULT);
  $res = pager_query($sql, $items, 0, NULL, $args);
  while ($rec = db_fetch_array($res)){
    $table[] = array(
      l($rec['sitename'], $rec['scheme'] . '://'.$rec['subdomain'].'/'),
      l(t('edit'), 'admin/build/dominion/list/' . $rec['domain_id'] . '/edit').' '.
      l(t('delete'), 'admin/build/dominion/list/' . $rec['domain_id'] . '/delete') . ' '.
      l(t('editors'), 'admin/build/dominion/list/' . $rec['domain_id'] . '/users')
    );
  }
  $output .= drupal_get_form('dominion_admin_search_form');
  if (empty($table)){
    $output .= t('No subsites found.');
  } else {
    $output .= theme_table(array(t('Name'), t('Operations')), $table);
    $output .= theme('pager');
  }

  return $output;
}

function dominion_admin_search_form(&$form_state) {
  $form = array();

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsible' => TRUE,
    '#collapsed' => !isset($_GET['search']),
  );

  $form['search']['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Keywords'),
    '#default_value' => isset($_GET['search']) ? $_GET['search'] : '',
  );

  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

function dominion_admin_search_form_submit($form, &$form_state) {
  $query = $form_state['values']['search'] ? 'search=' . urlencode($form_state['values']['search']) : '';
  drupal_goto('admin/build/dominion', $query);
}

/**
 * Menu callback.
 *
 * Generates the admin settings form.
 */
function dominion_admin_settings(){
  $form = array();

  $form[DOMINION_VAR_DOMAIN_SUFFIX] = array(
    '#type' => 'textfield',
    '#title' => t('Default host suffix'),
    '#required' => TRUE,
    '#default_value' => variable_get(DOMINION_VAR_DOMAIN_SUFFIX, DOMINION_VAR_DOMAIN_SUFFIX_DEFAULT),
    '#description' => t('Domains for subsites are suffixed by this string by default.'),
  );

  $form[DOMINION_VAR_ALLOW_TLD] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow custom domains for subsites'),
    '#default_value' => variable_get(DOMINION_VAR_ALLOW_TLD, DOMINION_VAR_ALLOW_TLD_DEFAULT),
  );

  $sql = 'SELECT rid, name FROM {role} ORDER BY name ASC';
  $res = db_query($sql);
  $options = array();
  while ($rec = db_fetch_array($res)) {
    $options[$rec['rid']] = $rec['name'];
  }
  $form[DOMINION_VAR_EDITOR_ROLES] = array(
    '#type' => 'checkboxes',
    '#title' => t('Editor roles'),
    '#options' => $options,
    '#default_value' => variable_get(DOMINION_VAR_EDITOR_ROLES, array()),
    '#description' => t('Roles which can be selected when adding an editor to a subsite.'),
  );

  $form[DOMINION_VAR_DEFAULT_FRONTPAGE] = array(
    '#type' => 'textfield',
    '#title' => t('Default frontpage'),
    '#default_value' => variable_get(DOMINION_VAR_DEFAULT_FRONTPAGE, ''),
    '#description' => t('Internal path. Leave empty to use site default.'),
  );
  
  $form[DOMINION_VAR_ALLOW_CUSTOM_FRONTPAGE] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow custom frontpage for subsites'),
    '#default_value' => variable_get(DOMINION_VAR_ALLOW_CUSTOM_FRONTPAGE, DOMINION_VAR_ALLOW_CUSTOM_FRONTPAGE_DEFAULT),
  );

  $options = array('' => t('Disable')) + node_get_types('names');
  $form[DOMINION_VAR_FRONTPAGE_NODETYPE] = array(
    '#type' => 'select',
    '#title' => t('Nodetype for static frontpage'),
    '#options' => $options,
    '#default_value' => variable_get(DOMINION_VAR_FRONTPAGE_NODETYPE, ''),
  );

  $form[DOMINION_VAR_PATH_FRONTPAGE] = array(
    '#type' => 'textfield',
    '#title' => t('Static frontpage path'),
    '#default_value' => variable_get(DOMINION_VAR_PATH_FRONTPAGE, DOMINION_VAR_PATH_FRONTPAGE_DEFAULT),
    '#description' => t('Path to use for the static subsite specific frontpage.'),
    '#required' => TRUE,
  );

  $form[DOMINION_VAR_DEFAULT_MENU] = array(
    '#type' => 'textarea',
    '#title' => t('Default menu'),
    '#default_value' => variable_get(DOMINION_VAR_DEFAULT_MENU, DOMINION_VAR_DEFAULT_MENU_DEFAULT),
    '#description' => t('Default menu to create for new subsites. One item per line, each having a path, title and optional a required function name separated by pipes.'),
    '#wysiwyg' => FALSE,
  );

  $form[DOMINION_VAR_ITEMS_IN_OVERVIEW] = array(
    '#type' => 'textfield',
    '#title' => t('Number of items in overview'),
    '#default_value' => variable_get(DOMINION_VAR_ITEMS_IN_OVERVIEW, DOMINION_VAR_ITEMS_IN_OVERVIEW_DEFAULT),
    '#required' => TRUE,
  );

  $form[DOMINION_VAR_AUTO_DELETE_NODES] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete old nodes'),
    '#default_value' => variable_get(DOMINION_VAR_AUTO_DELETE_NODES, DOMINION_VAR_AUTO_DELETE_NODES_DEFAULT),
    '#description' => t('Automatically delete nodes in cron from deleted subsites.'),
  );

  $form['#submit'][] = 'dominion_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Form submit handler.
 */
function dominion_admin_settings_submit($form, &$form_state) {
  dominion_path(DOMINION_VAR_PATH_FRONTPAGE, $form_state['values'][DOMINION_VAR_PATH_FRONTPAGE], TRUE);
  menu_rebuild();
}

/**
 * Menu callback for the domain users page.
 */
function dominion_admin_users($domain_id) {
  $output = '';

  if (!$subsite = dominion_get_info($domain_id)) {
    drupal_not_found();
  }

  drupal_set_title(t('Editors for %name', array('%name' => $subsite->name)));

  $output .= drupal_get_form('dominion_admin_add_user_form', $domain_id);

  $sql = 'SELECT u.uid, u.name, u.mail
  FROM {domain_editor} de
  JOIN {users} u ON (u.uid = de.uid)
  WHERE de.domain_id = %d
  ORDER BY u.name ASC';
  $res = db_query($sql, $subsite->domain_id);
  while ($rec = db_fetch_array($res)){
    $sql = 'SELECT r.name
    FROM {users_roles} ur
    JOIN {role} r ON (r.rid = ur.rid)
    WHERE ur.uid = %d';
    $res2 = db_query($sql, $rec['uid']);
    $roles = array();
    while ($rec2 = db_fetch_array($res2)){
      $roles[] = t($rec2['name']);
    }
    $roles = implode(', ', $roles);
    $table[] = array(
      $rec['name'],
      $rec['mail'],
      $roles,
      l(t('Remove from list'), 'admin/build/dominion/list/' . $subsite->domain_id . '/remove-user/' . $rec['uid'])
    );
  }
  if (empty($table)){
    $output .= t('There are no editors for this domain yet.');
  } else {
    $output .= theme_table(array(t('Name'), t('Mail account'), t('Roles'), t('Operations')), $table);
  }

  return $output;
}

function dominion_admin_add_user_form(&$form_state, $domain_id) {
  $form['#domain_id'] = $domain_id;

  $form['add_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add editor'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['add_user']['intro'] = array(
    '#value' => '<p>'.t('Add an editor by name or email.').'</p>'
  );

  $form['add_user']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#autocomplete_path' => 'admin/build/dominion/list/users/autocomplete/username'
  );

  $form['add_user']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail account'),
    '#autocomplete_path' => 'admin/build/dominion/list/users/autocomplete/email'
  );

  $editor_roles = variable_get(DOMINION_VAR_EDITOR_ROLES, array());
  $options = array();
  foreach ($editor_roles as $rid) {
    if ($rid) {
      $options[$rid] = _dominion_get_role_name($rid);
    }
  }
  if ($options) {
    $form['add_user']['roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Additional roles'),
      '#options' => $options,
      '#description' => t('Roles checked here are added to the user. No roles will be deleted if the user already have one or more of these roles.')
    );
  }

  $form['add_user']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

function dominion_admin_add_user_form_validate($form, &$form_state) {
  $name = $form_state['values']['name'];
  $email = $form_state['values']['email'];
  if (empty($name) && empty($email)) {
    form_set_error('', t('Specify a username or mailaddress.'));
  } else {
    if (empty($name)){
      $res = db_query('SELECT uid FROM {users} WHERE mail = \'%s\'', $email);
    } elseif (empty($email)){
      $res = db_query('SELECT uid FROM {users} WHERE name = \'%s\'', $name);
    } else {
      $res = db_query('SELECT uid FROM {users} WHERE name = \'%s\' AND mail = \'%s\'', $name, $email);
    }
    if (!$rec = db_fetch_array($res)){
      form_set_error('', t('No user has been found with the given username or mailaddress.'));
    }
  }
}

/**
 * Get the role name by role id
 */
function _dominion_get_role_name($rid){
  $sql = 'SELECT name FROM {role} WHERE rid = %d';
  return db_result(db_query($sql, $rid));
}

function dominion_admin_add_user_form_submit($form, &$form_state) {
  if (!$subsite = dominion_get_info($form['#domain_id'])){
    drupal_not_found();
    return;
  }
  $name = $form_state['values']['name'];
  $email = $form_state['values']['email'];
  $roles = isset($form_state['values']['roles']) ? $form_state['values']['roles'] : array();
  if (empty($name)){
    $res = db_query('SELECT uid FROM {users} WHERE mail = \'%s\'', $email);
  } elseif (empty($email)){
    $res = db_query('SELECT uid FROM {users} WHERE name = \'%s\'', $name);
  } else {
    $res = db_query('SELECT uid FROM {users} WHERE name = \'%s\' AND mail = \'%s\'', $name, $email);
  }
  if ($rec = db_fetch_array($res)){
    $new_roles = $account->roles;
    foreach ($roles as $rid => $value) {
      if ($value) {
        $new_roles[$rid] = _dominion_get_role_name($rid);
      }
    }
    if ($account = user_load(array('uid' => (int) $rec['uid']))){
      user_save($account, array('roles' => $new_roles));
    }

    $sql = 'INSERT INTO {domain_editor} (uid, domain_id) VALUES (%d, %d)';
    db_query($sql, $rec['uid'], $subsite->domain_id);

    drupal_set_message(t('The user was added as an editor for %name.', array('%name' => $subsite->name)));
  } else {
    drupal_set_message(t('User was not found'), 'error');
  }
}

function dominion_admin_delete_user_form($form_state, $domain_id, $uid) {
  if (!$subsite = dominion_get_info($domain_id)){
    drupal_not_found();
  }
  if (!$user = user_load($uid)){
    drupal_not_found();
  }

  $form = array();
  $form['#domain_id'] = $domain_id;
  $form['#uid'] = $uid;
  $form['domain'] = array(
    '#value' => t('Do you want to remove %name from the editors list for %domain?', array('%name' => $user->name, '%domain' => $subsite->name))
  );
  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete user from editors list')
  );
  $form['buttons']['cancel'] = array(
    '#value' => l(t('Cancel'), 'admin/build/dominion/list/' . $domain_id . '/users')
  );
  return $form;
}

function dominion_admin_delete_user_form_submit($form, &$form_state){
  if (!$subsite = dominion_get_info($form['#domain_id'])){
    drupal_not_found();
  }
  if (!$user = user_load($form['#uid'])){
    drupal_not_found();
  }

  $sql = 'DELETE FROM {domain_editor} WHERE domain_id = %d AND uid = %d';
  db_query($sql, $form['#domain_id'], $form['#uid']);

  drupal_set_message(t('The user %name has been deleted from the editors list for %domain.', array('%name' => $user->name, '%domain' => $subsite->name)));

  $form_state['redirect'] = 'admin/build/dominion/list/' . $form['#domain_id'] . '/users';
}

function dominion_admin_users_autocomplete_username($string = '') {
	if($string) {
		$result = db_query(db_rewrite_sql("SELECT uid, name FROM {users} WHERE name LIKE '%s%%' ORDER BY name LIMIT 25"), $string);
		$output = array();
		while($row = db_fetch_object($result)) {
			$output[$row->name] = check_plain($row->name);
 		}
		drupal_json($output);
	} else {
	  drupal_json(array());
	}
}

function dominion_admin_users_autocomplete_email($string = '') {
	if($string) {
		$result = db_query(db_rewrite_sql("SELECT mail FROM {users} WHERE mail LIKE '%s%%' ORDER BY mail LIMIT 25"), $string);
		$output = array();
		while($row = db_fetch_object($result)) {
			$output[$row->mail] = check_plain($row->mail);
 		}
		drupal_json($output);
	} else {
	  drupal_json(array());
	}
}