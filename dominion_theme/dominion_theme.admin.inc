<?php

function dominion_theme_admin_settings() {
  $form = array();

  $form['rebuild'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rebuild CSS files'),
    '#description' => t('With this button you can rebuild the CSS files for all subsites. You may need to run this after updating modules or themes.')
  );
  $form['rebuild']['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild CSS files'),
  );

  $sql = 'SELECT name FROM {system} WHERE type = \'theme\' AND status = 1 ORDER BY name ASC';
  $res = db_query($sql);
  $options = array();
  while ($rec = db_fetch_array($res)) {
    $options[$rec['name']] = $rec['name'];
  }
  $form[DOMINION_THEME_VAR_THEMES] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available themes'),
    '#options' => $options,
    '#default_value' => variable_get(DOMINION_THEME_VAR_THEMES, array()),
    '#description' => t('Themes to choose from when creating a subsite.'),
  );

  $form[DOMINION_THEME_VAR_DEFAULT_THEME] = array(
    '#type' => 'select',
    '#title' => t('Default theme'),
    '#options' => $options,
    '#default_value' => variable_get(DOMINION_THEME_VAR_DEFAULT_THEME, ''),
    '#description' => t('Default theme for new subsites.'),
  );

  $options = array('' => t('Disable'));
  if (module_exists('imagecache')) {
    $sql = 'SELECT presetname FROM {imagecache_preset} ORDER BY presetname ASC';
    $res = db_query($sql);
    while ($rec = db_fetch_array($res)) {
      $options[$rec['presetname']] = $rec['presetname'];
    }
  }
  $form[DOMINION_THEME_VAR_IMAGECACHE] = array(
    '#type' => 'select',
    '#title' => t('Imagecache preset for thumbnails'),
    '#options' => $options,
    '#default_value' => variable_get(DOMINION_THEME_VAR_IMAGECACHE, ''),
    '#description' => t('Used for thumbnails of theme files.'),
  );

  $form['#submit'][] = 'dominion_theme_admin_settings_submit';

  return system_settings_form($form);
}

function dominion_theme_admin_settings_validate($form, &$form_state) {
  $enabled_themes = array();
  foreach ($form_state['values'][DOMINION_THEME_VAR_THEMES] as $theme) {
    if ($theme) {
      $enabled_themes[] = $theme;
    }
  }
  if (!in_array($form_state['values'][DOMINION_THEME_VAR_DEFAULT_THEME], $enabled_themes)) {
    form_set_error(DOMINION_THEME_VAR_DEFAULT_THEME, t('The default theme must be available.'));
  }
}

function dominion_theme_admin_settings_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-rebuild') {
    $sql = 'SELECT domain_id FROM {dominion}';
    $res = db_query($sql);
    while ($rec = db_fetch_array($res)) {
      if ($subsite = dominion_get_info($rec['domain_id'])) {
        if ($theme_data = _dominion_theme_apply_configuration($subsite->domain_id, $subsite->theme, $subsite->theme_config)) {
          extract($theme_data);
        }
        else {
          $stylesheets = array();
          $files = array();
        }
        $stylesheets_serialized = serialize($stylesheets);
        $files_serialized = serialize($files);
        $sql = 'UPDATE {dominion_theme} SET stylesheets = \'%s\', files = \'%s\' WHERE domain_id = %d';
        db_query($sql, $stylesheets_serialized, $files_serialized, $subsite->domain_id);
        cache_clear_all("info:" . $subsite->domain_id, 'cache_dominion');
      }
    }
  }
}
