<?php

function dominion_nodewords_admin_settings(&$form_state) {
  $form = array();

  $form[DOMINION_NODEWORDS_VAR_DEFAULT_DESCRIPTION] = array(
    '#type' => 'textfield',
    '#title' => t('Default description'),
    '#default_value' => variable_get(DOMINION_NODEWORDS_VAR_DEFAULT_DESCRIPTION, ''),
    '#description' => t('Default value for description meta tag.'),
  );

  $form[DOMINION_NODEWORDS_VAR_DEFAULT_KEYWORDS] = array(
    '#type' => 'textfield',
    '#title' => t('Default keywords'),
    '#default_value' => variable_get(DOMINION_NODEWORDS_VAR_DEFAULT_KEYWORDS, ''),
    '#description' => t('Default value for keywords meta tag.'),
  );

  $form[DOMINION_NODEWORDS_VAR_OVERRIDE_NODEWORDS] = array(
    '#type' => 'checkbox',
    '#title' => t('Override nodewords'),
    '#default_value' => variable_get(DOMINION_NODEWORDS_VAR_OVERRIDE_NODEWORDS, DOMINION_NODEWORDS_VAR_OVERRIDE_NODEWORDS_DEFAULT),
    '#description' => t('Use the description and keywords set for the domain, even if it\'s different from the values provided by nodewords.'),
  );

  return system_settings_form($form);
}