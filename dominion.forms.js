Drupal.behaviors.dominion = function(context) {
  if (document.getElementById('edit-domain-type-subdomain').checked) {
    $('#edit-subdomain-wrapper').show();
    $('#edit-domain-wrapper').hide();
  }
  else {
    $('#edit-subdomain-wrapper').hide();
    $('#edit-domain-wrapper').show();
  }

  $('#edit-domain-type-subdomain', context).change(function (obj) {
    $('#edit-subdomain-wrapper').show();
    $('#edit-domain-wrapper').hide();
  });
  $('#edit-domain-type-domain', context).change(function (obj) {
    $('#edit-subdomain-wrapper').hide();
    $('#edit-domain-wrapper').show();
  });
}