<?php

function dominion_apachesolr_admin_settings(&$form_state) {
  $form = array();

  $form[DOMINION_APACHESOLR_VAR_FIELD_DOMAIN_ID] = array(
    '#type' => 'textfield',
    '#title' => t('Solr domain id field'),
    '#default_value' => variable_get(DOMINION_APACHESOLR_VAR_FIELD_DOMAIN_ID, DOMINION_APACHESOLR_VAR_FIELD_DOMAIN_ID_DEFAULT),
    '#description' => t('Name for field in solr schema.xml where the domain ids are stored.'),
  );

  $form[DOMINION_APACHESOLR_VAR_FIELD_DOMAIN_SITE] = array(
    '#type' => 'textfield',
    '#title' => t('Solr domain site field'),
    '#default_value' => variable_get(DOMINION_APACHESOLR_VAR_FIELD_DOMAIN_SITE, DOMINION_APACHESOLR_VAR_FIELD_DOMAIN_SITE_DEFAULT),
    '#description' => t('Name for field in solr schema.xml where the domain site value is stored.'),
  );

  $form[DOMINION_APACHESOLR_VAR_FILTER] = array(
    '#type' => 'checkbox',
    '#title' => t('Filter search results to current subsite'),
    '#default_value' => variable_get(DOMINION_APACHESOLR_VAR_FILTER, DOMINION_APACHESOLR_VAR_FILTER_DEFAULT),
    '#description' => t('Does not apply for domains which are not handled by dominion.'),
  );

  return system_settings_form($form);
}
